<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="MySQL">
        <meta name="keywords" content="MySQL, form, PHP, film, movie">
        <meta name="author" content="Luka Matkovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <title>MySQL-Movies</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Della+Respira" rel="stylesheet">
    </head>
    <body>
        <div class="menu"><?php include("inc/menu.html"); ?></div>
        <div class="main">
            <?php

            define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
            require('inc/config.php');

            if(!isset($_GET["link"])) $_GET["link"]="index";

            switch($_GET["link"])
            {
                case "index":
                include("inc/film.php");
                break;

                case "genre":
                include("inc/genre.php");
                break;

                case "schedule":
                include("inc/schedule.php");
                break;

                case "form":
                include("inc/form.php");
                break;


                default:
                include("inc/film.php");
                break;
            }
            mysqli_close($connection);
            ?>
        </div>
    </body>
</html>