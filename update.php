<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="MySQL">
        <meta name="keywords" content="MySQL, form, PHP, film, movie">
        <meta name="author" content="Luka Matkovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <title>MySQL-Movies</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Della+Respira" rel="stylesheet">
    </head>
    <body>
        <div class="menu"><?php include("inc/menu.html"); ?></div>
        <div class="main">

            <?php
            define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
            require('inc/config.php');

            if(isset($_GET["id"])) { $id=$_GET["id"]; }
            
            $sql1 = "SELECT * FROM film INNER JOIN genre ON film.genre_id=genre.genre_id";
            $result1 = mysqli_query($connection,$sql1) or die(mysqli_error($connection));

            if(mysqli_num_rows($result1)>=0){
            
                $row1=mysqli_fetch_array($result1,MYSQLI_ASSOC);
                //$id = $_GET["id"];
                $name = $row1['name'];
                $genre_id = $row1['genre_name'];
                $duration = $row1['duration'];

                while ($row1=mysqli_fetch_array($result1,MYSQLI_ASSOC))
                {
                    $row1=mysqli_fetch_array($result1,MYSQLI_ASSOC);
                    //$id = $_GET["id"];
                    $name = $row1['name'];
                    $genre_id = $row1['genre_id'];
                    $duration = $row1['duration'];
                }
            }
                mysqli_close($connection);
            ?>

        <form action="realUpdate.php" method="post">

            <label for="id">Film No. <?php echo $id; ?></label>
            <div class="hidden"><input type="number" name="id" min="1" max="20" value="<?php echo $id; ?>"><br></div>

            <label for="name">Film Name</label>
            <input type="text" name="name" value="<?php echo $name; ?>"><br>

            <label for="genre_id">Genre ID</label>
            <input type="number" name="genre_id" min="1" max="5" value="<?php echo $genre_id; ?>"><br>

            <label for="duration">Duration (minutes)</label>
            <input type="number" name="duration" value="<?php echo $duration; ?>"><br>

            <input type="submit" value="Submit">
        </form>
        <form action="delete.php" method="post">
            <div class="hidden"><input type="number" name="id" min="1" max="20" value="<?php echo $id; ?>"><br></div>
            <input type="submit" value="Delete">
        </form>
    </body>
</html>